import 'package:flutter/material.dart';
import 'package:wiget_week_four/main.dart';

class PageTwo extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    print(RouteService.currentRoute);


    return Scaffold(
      appBar: AppBar(title: Text('Page 2'),),
      body: Container(color: Colors.blue,),
    );
  }
}
class PageThree extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    print(RouteService.currentRoute);
    return Scaffold(
      appBar: AppBar(title: Text('Page 3'),),
      body: Container(color: Colors.red,),
    );
  }
}
