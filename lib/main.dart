import 'package:flutter/material.dart';
import 'package:wiget_week_four/page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: RouteService.onGenerateRoute,
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 1'),
      ),
      drawer: Drawer(),
      body: Center(
        child: Column(
          children: [
            SafeArea(
              child: ClipOval(
                child: Container(
                  width: 100,
                  height: 100,
                  color: Colors.white,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed('Page');
                    },
                    child: Text('next'),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class RouteService {

  static String? currentRoute;

  static String? prevRoute;

  static Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    prevRoute = currentRoute;

    Route<dynamic>? route;

    if (settings.name == 'Page') {
      route = MaterialPageRoute(builder: (context) => PageTwo());
    } else {
      route = MaterialPageRoute(builder: (context) => PageThree());
    }
    print(RouteService.prevRoute);
    currentRoute = settings.name;
    return route;
  }
}
